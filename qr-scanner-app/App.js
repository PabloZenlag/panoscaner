import React, { useState, useEffect } from 'react';
import { Text, View, Button, TextInput, StyleSheet, Dimensions } from 'react-native';
import { BarCodeScanner } from 'expo-barcode-scanner';

export default function App() {
  const [hasPermission, setHasPermission] = useState(null);
  const [scanned, setScanned] = useState(false);
  const [scannedData, setScannedData] = useState('');
  const [operatorName, setOperatorName] = useState('');
  const [operatorSurname, setOperatorSurname] = useState('');
  const [operatorEntered, setOperatorEntered] = useState(false);

  useEffect(() => {
    (async () => {
      const { status } = await BarCodeScanner.requestPermissionsAsync();
      setHasPermission(status === 'granted');
    })();
  }, []);

  const handleBarCodeScanned = async ({ type, data }) => {
    setScanned(true);
    setScannedData(data);
  };

  const handleOperatorSubmit = () => {
    if (operatorName && operatorSurname) {
      setOperatorEntered(true);
    } else {
      alert('Por favor ingrese el nombre y apellido del operario.');
    }
  };

  const handleSendData = async () => {
    try {
      const now = new Date();
      const formattedDate = `${now.getFullYear()}-${(now.getMonth() + 1).toString().padStart(2, '0')}-${now.getDate().toString().padStart(2, '0')}`;
      const formattedTime = `${now.getHours().toString().padStart(2, '0')}:${now.getMinutes().toString().padStart(2, '0')}:${now.getSeconds().toString().padStart(2, '0')}`;
      const response = await fetch('http://192.168.58.205:5001/scan', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({ 
          date: formattedDate,
          time: formattedTime,
          operator: { 
            name: operatorName, 
            surname: operatorSurname 
          },
          qrData: scannedData 
        })
      });
      const responseData = await response.json();
      console.log(responseData);
      alert('Datos enviados correctamente al servidor.');
    } catch (error) {
      console.error('Error al enviar los datos:', error);
      alert('Error al enviar los datos al servidor.');
    }
  };

  if (!operatorEntered) {
    return (
      <View style={styles.container}>
        <Text style={styles.header}>Ingrese los datos del operario</Text>
        <TextInput
          style={styles.input}
          placeholder="Nombre"
          value={operatorName}
          onChangeText={setOperatorName}
        />
        <TextInput
          style={styles.input}
          placeholder="Apellido"
          value={operatorSurname}
          onChangeText={setOperatorSurname}
        />
        <Button title="Ingresar" onPress={handleOperatorSubmit} color="#FFA500" />
      </View>
    );
  }

  if (hasPermission === null) {
    return <Text style={styles.text}>Solicitando permiso para usar la cámara...</Text>;
  }
  if (hasPermission === false) {
    return <Text style={styles.text}>No se ha concedido permiso para usar la cámara.</Text>;
  }

  return (
    <View style={styles.container}>
      <Text style={styles.header}>PanorScan</Text>
      <View style={styles.barcodeContainer}>
        <BarCodeScanner
          onBarCodeScanned={scanned ? undefined : handleBarCodeScanned}
          style={styles.barcode}
        />
      </View>
      {scanned && (
        <Button title={'Escanear de nuevo'} onPress={() => setScanned(false)} color="#FFA500" />
      )}
      <Text style={styles.text}>{scannedData}</Text>
      <View style={styles.buttonContainer}>
        <Button title="Volver atrás" onPress={() => setOperatorEntered(false)} color="#FFA500" />
        <Button title="Enviar datos" onPress={handleSendData} color="#FFA500" />
      </View>
    </View>
  );
}

const { width } = Dimensions.get('window');
const barcodeSize = width * 0.6;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#000',
    padding: 20,
  },
  header: {
    fontSize: 24,
    color: '#FFA500',
    marginBottom: 20,
  },
  input: {
    width: '80%',
    height: 40,
    borderColor: '#FFA500',
    borderWidth: 1,
    borderRadius: 5,
    paddingHorizontal: 10,
    marginBottom: 10,
    backgroundColor: '#FFF',
  },
  barcodeContainer: {
    width: barcodeSize,
    height: barcodeSize,
    justifyContent: 'center',
    alignItems: 'center',
    overflow: 'hidden',
    borderRadius: 15,
    borderWidth: 2,
    borderColor: '#FFA500',
  },
  barcode: {
    width: '100%',
    height: '100%',
  },
  text: {
    fontSize: 18,
    textAlign: 'center',
    margin: 10,
    color: '#FFA500',
    backgroundColor: '#000',

  },
  buttonContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '100%',
    marginTop: 20,
  },
});






