import tkinter as tk
from tkinter import messagebox
from tkinter import ttk
from flask import Flask, request, jsonify
import threading
from datetime import datetime
import pandas as pd

app = Flask(__name__)

# Lista global para almacenar los datos recibidos
received_data_list = []

# Función para actualizar la tabla con los datos recibidos
def update_table():
    # Borra todos los elementos de la tabla
    for row in tree.get_children():
        tree.delete(row)

    # Inserta los datos actualizados en la tabla
    for data in received_data_list:
        tree.insert('', 'end', values=data)

# Función para actualizar los datos globales y luego actualizar la tabla
def update_data(data):
    # Obtener la fecha y hora actuales
    current_datetime = datetime.now()
    current_date = current_datetime.strftime('%Y-%m-%d')
    current_time = current_datetime.strftime('%H:%M:%S')
    # Extraer los datos relevantes
    operator_name = data.get('operator', {}).get('name', 'Desconocido')
    qr_info = data.get('qrData', 'Sin información')
    # Agregar los nuevos datos a la lista
    received_data_list.append((current_date, current_time, operator_name, qr_info))
    # Programa la actualización de la tabla desde el hilo principal
    root.after(0, update_table)

@app.route('/scan', methods=['POST'])
def scan():
    data = request.json
    print("Datos escaneados:", data)
    # Actualiza los datos globales y la tabla
    update_data(data)
    # Envía una respuesta al cliente
    return jsonify({'message': 'Datos recibidos correctamente'})

def authenticate(username, password):
    # Aquí puedes implementar tu lógica de autenticación
    # Por ahora, simplemente verificaremos si el nombre de usuario y la contraseña son "admin"
    return username == "admin" and password == "admin"

def login():
    username = entry_username.get()
    password = entry_password.get()
    if authenticate(username, password):
        messagebox.showinfo("Éxito", "Inicio de sesión exitoso")
        entry_username.delete(0, tk.END)
        entry_password.delete(0, tk.END)
        frame_login.destroy()  # Destruye la ventana de inicio de sesión
        root.deiconify()  # Muestra la ventana principal
    else:
        messagebox.showerror("Error", "Nombre de usuario o contraseña incorrectos")

def print_data():
    # Aquí puedes agregar la lógica para imprimir los datos
    pass

def update_data_button():
    # Esta función actualiza los datos en la tabla
    update_table()

def save_to_excel():
    # Crear un DataFrame a partir de los datos recibidos
    df = pd.DataFrame(received_data_list, columns=['Fecha', 'Hora', 'Operador', 'QR'])
    
    # Obtener la ruta de destino del archivo Excel (puedes ajustarla según tus necesidades)
    file_path = 'datos_escaneados.xlsx'
    
    # Guardar el DataFrame en un archivo Excel
    df.to_excel(file_path, index=False)
    
    # Mostrar un mensaje de éxito
    messagebox.showinfo("Éxito", f"Datos guardados en {file_path}")

def run_tk():
    # Configuración de la ventana Tkinter
    global root, tree, frame_login, entry_username, entry_password

    root = tk.Tk()
    root.title("Datos Recibidos")

    # Establecer colores de fondo y texto
    root.configure(bg='#000')
    ttk.Style().configure('Treeview', background='#FFF', foreground='#000')
    ttk.Style().configure('TButton', background='#FFA500', foreground='#000')
    ttk.Style().configure('TLabel', background='#000', foreground='#FFA500')

    # Ocultar la ventana principal hasta que se inicie sesión correctamente
    root.withdraw()

    # Ventana para el inicio de sesión
    frame_login = tk.Toplevel(root)
    frame_login.title("Inicio de sesión")
    frame_login.configure(bg='#000')

    label_username = tk.Label(frame_login, text="Usuario:", bg='#000', fg='#FFA500')
    label_username.grid(row=0, column=0, padx=5, pady=5)
    entry_username = tk.Entry(frame_login, bg='#FFF', fg='#000')
    entry_username.grid(row=0, column=1, padx=5, pady=5)

    label_password = tk.Label(frame_login, text="Contraseña:", bg='#000', fg='#FFA500')
    label_password.grid(row=1, column=0, padx=5, pady=5)
    entry_password = tk.Entry(frame_login, show="*", bg='#FFF', fg='#000')
    entry_password.grid(row=1, column=1, padx=5, pady=5)

    button_login = tk.Button(frame_login, text="Iniciar sesión", command=login, bg='#FFA500', fg='#000')
    button_login.grid(row=2, columnspan=2, padx=5, pady=5)

    # Crear un marco para los botones superiores
    frame_buttons = tk.Frame(root, bg='#000')
    frame_buttons.pack(side="top", fill="x")

    # Botón para imprimir los datos (puedes implementar la lógica si lo necesitas)
    button_print = tk.Button(frame_buttons, text="Imprimir datos", command=print_data, bg='#FFA500', fg='#000')
    button_print.pack(side="left", padx=5, pady=5)

    # Botón para guardar los datos en un archivo Excel
    button_save = tk.Button(frame_buttons, text="Guardar datos", command=save_to_excel, bg='#FFA500', fg='#000')
    button_save.pack(side="left", padx=5, pady=5)

    # Botón para actualizar los datos en la tabla
    button_update = tk.Button(frame_buttons, text="Actualizar datos", command=update_data_button, bg='#FFA500', fg='#000')
    button_update.pack(side="left", padx=5, pady=5)

    # Creación del widget Treeview para mostrar los datos en forma de tabla
    tree = ttk.Treeview(root, columns=('Fecha', 'Hora', 'Operador', 'QR'), show='headings')
    tree.heading('Fecha', text='Fecha')
    tree.heading('Hora', text='Hora')
    tree.heading('Operador', text='Operador')
    tree.heading('QR', text='Información de QR')
    tree.pack(expand=True, fill='both')

    # Ejemplo de datos iniciales
    initial_data = ("¡Bienvenido!", "", "", "")
    received_data_list.append(initial_data)
    update_table()

    # Ejecutar la ventana
    root.mainloop()

if __name__ == '__main__':
    # Hilo para ejecutar la interfaz gráfica (Tkinter)
    tkinter_thread = threading.Thread(target=run_tk)
    tkinter_thread.daemon = True
    tkinter_thread.start()

    # Ejecutar el servidor Flask en el hilo principal
    app.run(debug=True, host='0.0.0.0', port=5001)

















